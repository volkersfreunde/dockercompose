# API-Testsetup, aber mit Docker

wirklich nur um das API zu testen. ATM implementiert: machines::* & machine::read, authenticate

* run `docker-compose up` in this directory
* [View the documentation](https://fab-access.readthedocs.io/en/v0.3/installation/server_docker.html)

open questions:
* shoudl I change the cert?
* how do I validate the server is working?
   * sample requests?
   * what to expect on port `59661`?


# setup tasmota
```
cd adapters
git clone https://gitlab.com/fabinfra/fabaccess/actors/tasmota.git
chmod +x tasmota/main.py 
```